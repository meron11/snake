//
// Created by Marcin Golonka on 12.11.16.
//

#include <stdlib.h>
#include "Point.h"

sf::RectangleShape Point::drawRandomPoint(sf::RenderWindow &window) {
    std::srand ( time(NULL) );
    int randX = ((rand()%(maxX-minX + 1) + minX)/pointSize) * pointSize;
    int randY = ((rand()%(maxY-minY + 1) + minY)/pointSize) * pointSize;


    sf::Vector2f position(randX,randY);


    return  drawPoint(position,window);
}

sf::RectangleShape Point::drawPoint(sf::Vector2<float> position, sf::RenderWindow &window) {
    sf::RectangleShape rectangle;
    rectangle.setSize(sf::Vector2f(pointSize,pointSize));
    rectangle.setFillColor(sf::Color::Green);
    rectangle.setPosition(position);

    window.draw(rectangle);
    return rectangle;
}
