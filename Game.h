//
// Created by Marcin Golonka on 06.11.16.
//

#ifndef SNAKE_GAME_H
#define SNAKE_GAME_H


#include <SFML/Graphics.hpp>
#include <iostream>
#include "Snake.h"


class Game {

public:
    Game();

private:
    const char* title = "Snake";
    sf::RenderWindow window;
    sf::Font font;
    bool isMenu = true;
    bool isGame,isGameOver,firstStartGame = false;

    sf::RectangleShape food;
    bool snakeCatchPoint = false;

    int score = 0;

    //todo migrate this to menu class
    void eventsMenu();



    void init();

    //game section
    void drawScore();
    void eventsGame();

    //snake
    Snake *snake;

    void gameOver();
};


#endif //SNAKE_GAME_H
