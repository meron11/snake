//
// Created by Marcin Golonka on 12.11.16.
//

#ifndef SNAKE_POINT_H
#define SNAKE_POINT_H


#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <stdlib.h>


class Point {

public:
    static sf::RectangleShape drawRandomPoint(sf::RenderWindow &window);
    static sf::RectangleShape drawPoint(sf::Vector2<float> position, sf::RenderWindow &window);

private:
    static const int minX=6;
    static const int maxX=784;
    static const int minY=41;
    static const int maxY=584;

    static const int pointSize = 10;

};


#endif //SNAKE_POINT_H
