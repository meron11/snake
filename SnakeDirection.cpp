//
// Created by Marcin Golonka on 13.11.16.
//

enum SnakeDirection{
    up = 1,
    left = 2,
    right = 3,
    down = 4
};