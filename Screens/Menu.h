//
// Created by Marcin Golonka on 12.11.16.
//

#ifndef SNAKE_MENU_H
#define SNAKE_MENU_H

#include <SFML/Graphics.hpp>

class Menu {

public:
    /// Draws menu
    /// \param window
    /// \param font
    static void drawMenu(sf::RenderWindow &window, sf::Font &font);

    /// Draw games logo
    /// \param window
    /// \param font
    static void drawLogo(sf::RenderWindow &window, sf::Font &font);

};


#endif //SNAKE_MENU_H
