//
// Created by Marcin Golonka on 12.11.16.
//

#include "Menu.h"
void Menu::drawMenu(sf::RenderWindow &window, sf::Font &font) {
    sf::Text text;
    text.setFont(font);
    text.setString("PUSH ENTER TO START");
    text.setPosition(sf::Vector2f(250,500));
    text.setCharacterSize(15);
    text.setFillColor(sf::Color::White);
    window.draw(text);
}


void Menu::drawLogo(sf::RenderWindow &window, sf::Font &font) {
    sf::Text text;
    text.setFont(font);
    text.setString("Snake");
    text.setPosition(sf::Vector2f(260,100));
    text.setCharacterSize(50);
    text.setFillColor(sf::Color::White);
    window.draw(text);
}