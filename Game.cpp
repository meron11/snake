//
// Created by mer on 06.11.16.
//

#include "Game.h"
#include "Screens/Menu.h"
#include "Point.h"


Game::Game() {

    window.create(sf::VideoMode(800, 600), title);
    window.setFramerateLimit(10); //todo slow this more property
    font.loadFromFile("resources/fonts/8bit.ttf");
    init();
}

void Game::init() {

    while (window.isOpen()) {
        window.clear();

        if (isMenu) {
            Menu::drawLogo(window, font);
            Menu::drawMenu(window, font);
            eventsMenu();
        }

        if (isGame) {
            drawScore();

            if (firstStartGame) {
                snake = new Snake(&window);
                this->food = Point::drawRandomPoint(window);
                firstStartGame = false;
            } else {
                eventsGame();
                if(snake->tick()){
                    isGameOver =true;
                    isGame = false;
                }
                if(snake->checkColisionWithHead(this->food))
                    snakeCatchPoint = true;
            }

            if (snakeCatchPoint) {
                this->score += 10;

                this->food = Point::drawRandomPoint(window);
                this->snake->addSegment();
                snakeCatchPoint = false;
            } else
                Point::drawPoint(this->food.getPosition(), window);

        }
        if(isGameOver)
            gameOver();
        window.display();
    }

}

void Game::eventsMenu() {
    sf::Event event;
    while (window.pollEvent(event)) {
        switch (event.type) {
            case sf::Event::KeyPressed:
                if (event.key.code == sf::Keyboard::Return) //if pressed enter
                {
                    this->isMenu = false;
                    this->isGame = true;
                    this->firstStartGame = true;
                }
                break;
        }
    }

}

void Game::drawScore() {
    sf::Text text;
    text.setFont(font);
    text.setString("SCORE\t" + std::to_string(this->score));
    text.setPosition(sf::Vector2f(0, 10));
    text.setCharacterSize(20);
    text.setFillColor(sf::Color::White);
    window.draw(text);

    //draw line, extract to method
    sf::Vertex line[] =
            {
                    sf::Vertex(sf::Vector2f(0, 40)),
                    sf::Vertex(sf::Vector2f(window.getSize().x, 40))
            };
    window.draw(line,2, sf::Lines);
}

void Game::eventsGame() {
    sf::Event event;
    while (window.pollEvent(event)) {

        if (event.type == sf::Event::KeyPressed)
            switch (event.key.code) {
                case sf::Keyboard::Key::Left:
                    snake->setDirection(left);
                    break;
                case sf::Keyboard::Right:
                    snake->setDirection(right);
                    break;
                case sf::Keyboard::Down:
                    snake->setDirection(down);
                    break;
                case sf::Keyboard::Up:
                    snake->setDirection(up);
                    break;
            }
    }
}

void Game::gameOver() {
    window.clear(sf::Color::Black);
    sf::Text text;
    text.setFont(font);
    text.setString("Game over\n\nScore " + std::to_string(score));
    text.setPosition(sf::Vector2f(160,100));
    text.setCharacterSize(50);
    text.setFillColor(sf::Color::White);
    window.draw(text);
}




