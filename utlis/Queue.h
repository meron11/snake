//
// Created by Marcin Golonka on 12.11.16.
//

#ifndef SNAKE_QUEUE_H
#define SNAKE_QUEUE_H


template< class T > class Queue {
public:
    Queue(int = 200);
    ~Queue()//destructor
    {delete [] values;}
    bool enQueue( T );
    T deQueue();
    bool isEmpty();
    bool isFull();
private:
    int size;
    T *values;
    int front;
    int back;
};


#endif //SNAKE_QUEUE_H
