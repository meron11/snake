//
// Created by Marcin Golonka on 12.11.16.
//

#include "Snake.h"

Snake::Snake(sf::RenderWindow *window) {

    this->window = window;

    //init snake
    int x = snakeStartX;
    for (int i = 0; i < snakeStartSize; i++) {
        pointsSnake.push_back(drawPoint(sf::Vector2<float>(x, snakeStartY)));
        x -= snakePointSize;
    }

    this->head = &pointsSnake.front();
}

sf::RectangleShape Snake::drawPoint(sf::Vector2f position) {

    sf::RectangleShape rectangle;
    rectangle.setSize(sf::Vector2f(snakePointSize, snakePointSize));
    rectangle.setFillColor(sf::Color::White);

    //rotate over screen(X axis)
    if(position.x > window->getSize().x)
        position.x = window->getSize().x - position.x;
    else if(position.x < 0)
        position.x = window->getSize().x + position.x;

    //rotate over screen(Y axis)
    if(position.y > window->getSize().y)
        position.y =  ((position.y + 30)- window->getSize().y);
    else if(position.y < 40)
        position.y = window->getSize().y - (position.y -40) ;


    rectangle.setPosition(position);

    window->draw(rectangle);

    return rectangle;
}

bool Snake::tick() {
    moveSnake();
    return checkColisionWithBody();
}

void Snake::moveSnake() {
    addPointInHead();
    pointsSnake.pop_back(); //remove tail
    redrawSnake();

}

void Snake::redrawSnake() {
    for (auto const &point: pointsSnake) {
        drawPoint(point.getPosition());
    }

}

SnakeDirection Snake::getDirection() const {
    return direction;
}

void Snake::setDirection(SnakeDirection direction) {
    if(this->direction + direction == 5) //sum enums, colision directions are giving sum 5
        return;

    this->direction = direction;
}

bool Snake::checkColision(const sf::Vector2f &vector1, const sf::Vector2f &vector2) {

    return (vector1 == vector2);
}

bool Snake::checkColisionWithBody() {

    for(int i = 2;i < this->pointsSnake.size();i++) { //head and first element is safe
        if(checkColision((*head).getPosition(), pointsSnake[i].getPosition()))
            return true;
    }
    return false;
}

bool Snake::checkColisionWithHead(sf::RectangleShape &point) {
    return checkColision((*this->head).getPosition(), point.getPosition());
}

void Snake::addSegment() {
    addPointInHead();
    redrawSnake();
}

void Snake::addPointInHead() {
    switch(direction){
        case right:{
            sf::Vector2f  Position = sf::Vector2<float>((*head).getPosition().x + snakePointSize, (*head).getPosition().y);
            pointsSnake.insert(pointsSnake.begin(), drawPoint(Position));
            head = &pointsSnake.front();
            break;
        }
        case left:{
            sf::Vector2f  Position = sf::Vector2<float>((*head).getPosition().x - snakePointSize, (*head).getPosition().y);

            pointsSnake.insert(pointsSnake.begin(), drawPoint(Position));
            head = &pointsSnake.front();
            break;
        }
        case up:{
            sf::Vector2f  Position = sf::Vector2<float>((*head).getPosition().x, (*head).getPosition().y - snakePointSize);
            pointsSnake.insert(pointsSnake.begin(), drawPoint(Position));
            head = &pointsSnake.front();

            break;
        }
        case down:{
            sf::Vector2f  Position = sf::Vector2<float>((*head).getPosition().x, (*head).getPosition().y + snakePointSize);
            pointsSnake.insert(pointsSnake.begin(), drawPoint(Position));
            head = &pointsSnake.front();
            break;
        }
    }
}
