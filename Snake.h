//
// Created by Marcin Golonka on 12.11.16.
//

#ifndef SNAKE_SNAKE_H
#define SNAKE_SNAKE_H


#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include "utlis/Queue.h"
#include <iostream>
#include <vector>
#include <cmath>
#include "SnakeDirection.cpp"

class Snake {
public:
    Snake(sf::RenderWindow *window);

    bool tick();


public:
    SnakeDirection getDirection() const;

    void setDirection(SnakeDirection direction);

    bool checkColisionWithHead(sf::RectangleShape &point);
    bool checkColision(const sf::Vector2f &vector1, const sf::Vector2f &vector2);


    void addSegment();

private:
    const int snakeStartSize = 30;
    const int snakePointSize = 10;
    const int snakeStartX = 300;
    const int snakeStartY = 150;
    SnakeDirection  direction = right;

    sf::RectangleShape *head;

    sf::RenderWindow *window;
    std::vector<sf::RectangleShape> pointsSnake;


    sf::RectangleShape drawPoint(sf::Vector2f position);

    void moveSnake();
    void redrawSnake();

    bool checkColisionWithBody();

    void addPointInHead();
};



#endif //SNAKE_SNAKE_H
